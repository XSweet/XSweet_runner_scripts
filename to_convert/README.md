Put your `.docx` files to convert in this directory. Nest them in a directory one level deep or they will not be converted.

* Correct: `alex/my_file.docx`
* Incorrect: `my_file.docx`
* Incorrect: `alex/cool/my_file.docx`
