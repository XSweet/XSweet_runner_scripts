require 'fileutils'
require 'os'

# dir = '/Users/atheg/Desktop/crawler/to_convert'
convertDir = Dir.getwd + "/to_convert"

book_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}

book_list.each do |book|

  docx_list = Dir["#{book}/*.docx"]

  docx_list.each do |docx|
    no_space =  docx.split(" ").join
    if docx != no_space
      FileUtils.mv(docx, no_space)
    end
    zip_name_no_ext = no_space.slice(0...-5)
    zip_name_ext = zip_name_no_ext + ".zip"
    FileUtils.cp(no_space, zip_name_ext)
    sleep(1)
    # %x`open #{zip_name_ext}`
    if OS.windows?
      #support for in powershell
      %x`powershell.exe Expand-Archive #{zip_name_ext} -DestinationPath #{zip_name_no_ext}`    
    else
      %x`unzip #{zip_name_ext} -d #{zip_name_no_ext}`
    end        
  end
  sleep(8)
  zip_list = Dir["#{book}/*.zip"]
  zip_list.each do |zip|
    FileUtils.rm zip
  end
end
