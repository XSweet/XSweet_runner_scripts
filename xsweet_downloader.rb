require 'httparty'
require 'fileutils'
require 'os'

new_file_xsweet = File.join(Dir.pwd, "xsweet.zip")
new_file_typescript = File.join(Dir.pwd, "typescript.zip")
new_file_htmlevator = File.join(Dir.pwd, "htmlevator.zip")

#changed the file mode 'w' to 'wb', windows only supports wb
downloaded_file = File.new(new_file_xsweet, "wb")
downloaded_file.write(HTTParty.get("https://gitlab.coko.foundation/XSweet/XSweet/repository/archive.zip?ref=master").body)
downloaded_file.close

downloaded_file = File.new(new_file_typescript, "wb")
downloaded_file.write(HTTParty.get("https://gitlab.coko.foundation/XSweet/editoria_typescript/repository/archive.zip?ref=master").body)
downloaded_file.close

downloaded_file = File.new(new_file_htmlevator, "wb")
downloaded_file.write(HTTParty.get("https://gitlab.coko.foundation/XSweet/HTMLevator/repository/archive.zip?ref=master").body)
downloaded_file.close

if OS.mac?
# on linux change open to xdg-open
    %x`open #{new_file_xsweet}`
    sleep(1)
    %x`open #{new_file_typescript}`
    sleep(1)
    %x`open #{new_file_htmlevator}`
    sleep(1)
elsif OS.windows?
# Windows powershell command to extract files
    %x`powershell.exe Expand-Archive #{new_file_xsweet} -DestinationPath (Get-Location).Path`
    sleep(1)
    %x`powershell.exe Expand-Archive #{new_file_typescript} -DestinationPath (Get-Location).Path`
    sleep(1)
    %x`powershell.exe Expand-Archive #{new_file_htmlevator} -DestinationPath (Get-Location).Path`
    sleep(1)  
else
    puts new_file_xsweet
    %x`xdg-open #{new_file_xsweet}`
    sleep(1)
    %x`xdg-open #{new_file_typescript}`
    sleep(1)
    %x`xdg-open #{new_file_htmlevator}`
    sleep(1)
end

typescript_name = Dir.glob("editoria_typescript*").pop
xsweet_name = Dir.glob("XSweet-*").pop
htmlevator_name = Dir.glob("HTMLevator-*").pop

puts "typescript_name"
puts typescript_name
puts "xsweet_name"
puts xsweet_name
puts "htmlevator_name"
puts htmlevator_name

#Instead of using '%x syntax' used the FileUtils to keep it simple

FileUtils.mv(typescript_name, "#{xsweet_name}/applications/typescript")
FileUtils.mv(htmlevator_name, "#{xsweet_name}/applications/htmlevator")

FileUtils.mkdir("#{xsweet_name}/scripts")

FileUtils.mv("./execute_chain.sh", "#{xsweet_name}/scripts")
FileUtils.mv("./execute_chain_production.sh", "#{xsweet_name}/scripts")

FileUtils.rm "xsweet.zip"
FileUtils.rm "typescript.zip"
FileUtils.rm "htmlevator.zip"

