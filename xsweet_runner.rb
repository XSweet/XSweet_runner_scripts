require 'os'

def execute_chain(rootPath, actualBookName, bookChapter)
  commandname = "" #For Mac/Linux
  if OS.windows?
    #For windows environment
    commandname = "powershell.exe"
  end

  # Note Saxon is included with this distribution, qv for license.
  saxonHE="java -jar ../lib/SaxonHE9-9-1-1J/saxon9he.jar"  # SaxonHE (XSLT 3.0 processor)
  # INITIAL EXTRACTION
  extract="../applications/docx-extract/docx-html-extract.xsl"
  # RUNS `docx-table-extract.xsl`
  notes="../applications/docx-extract/handle-notes.xsl"
  scrub="../applications/docx-extract/scrub.xsl"
  join="../applications/docx-extract/join-elements.xsl"
  collapsepara="../applications/docx-extract/collapse-paragraphs.xsl"
    
  # TAG PLAIN TEXT STRINGS THAT APPEAR TO BE URLS AS LINKS
  links="../applications/htmlevator/applications/hyperlink-inferencer/hyperlink-inferencer.xsl"

  # REBUILDS LISTS FROM WORD AS HTML
  promotelists="../applications/list-promote/PROMOTE-lists.xsl"
    # RUNS `mark-lists.xsl`, THEN `itemize-lists.xsl`

  # DETECTS PLAIN-TEXT NUMBERED LISTS
  detectlists="../applications/htmlevator/applications/list-detect/DETECT-ITEMIZE-LISTS.xsl"
    # RUNS `detect-numbered-lists.xsl`, THEN `itemize-detected-lists.xsl`, THEN `scrub-literal-numbering-lists.xsl`

  # HEADER PROMOTION BY OUTLINE LEVEL OR DISPLAY ATTRIBUTES
  headerchooseandpromote="../applications/htmlevator/applications/header-promote/header-promotion-CHOOSE.xsl"

  # MATH (OMML TO MML)
  math="../applications/math/xsweet_tei_omml2mml.xsl"

  # FINAL HTML CLEANUPS
  finalrinse="../applications/html-polish/final-rinse.xsl"

  # UNIVERSITY OF CALIFORNIA PRESS COPYEDITING CLEANUPS
  ucptext="../applications/htmlevator/applications/ucp-cleanup/ucp-text-macros.xsl"

  # UCP-SPECIFIC SPECIFIC ELEMENT MAPPING
  ucpmap="../applications/htmlevator/applications/ucp-cleanup/ucp-mappings.xsl"

  # EDITORIA TYPESCRIPT
  splitonbr="../applications/typescript/p-split-around-br.xsl"
  editoriabasic="../applications/typescript/editoria-basic.xsl"
  editoriareduce="../applications/typescript/editoria-reduce.xsl"

  # SERIALIZE TO HTML5
  xmltohtml5="../applications/html-polish/html5-serialize.xsl"
  
  %x`#{commandname} #{saxonHE} -xsl:#{extract} -s:#{rootPath}/#{bookChapter}/word/document.xml -o:../outputs/#{actualBookName}/#{bookChapter}-1EXTRACTED.xhtml`
  puts "Made #{bookChapter}-1EXTRACTED.xhtml" 

  %x`#{commandname} #{saxonHE} -xsl:#{notes} -s:../outputs/#{actualBookName}/#{bookChapter}-1EXTRACTED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-2NOTES.xhtml`
  puts "Made #{bookChapter}-2NOTES.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{scrub} -s:../outputs/#{actualBookName}/#{bookChapter}-2NOTES.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-3SCRUBBED.xhtml`
  puts "Made #{bookChapter}-3SCRUBBED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{join} -s:../outputs/#{actualBookName}/#{bookChapter}-3SCRUBBED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-4JOINED.xhtml`
  puts "Made #{bookChapter}-4JOINED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{collapsepara} -s:../outputs/#{actualBookName}/#{bookChapter}-4JOINED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-5COLLAPSED.xhtml`
  puts "Made #{bookChapter}-5COLLAPSED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{links} -s:../outputs/#{actualBookName}/#{bookChapter}-5COLLAPSED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-6LINKS.xhtml`
  puts "Made #{bookChapter}-6LINKS.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{promotelists} -s:../outputs/#{actualBookName}/#{bookChapter}-6LINKS.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-7PROMOTELISTS.xhtml`
  puts "Made #{bookChapter}-7PROMOTELISTS.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{detectlists} -s:../outputs/#{actualBookName}/#{bookChapter}-7PROMOTELISTS.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-8DETECTLISTS.xhtml`
  puts "Made #{bookChapter}-8DETECTLISTS.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{headerchooseandpromote} -s:../outputs/#{actualBookName}/#{bookChapter}-8DETECTLISTS.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-9HEADERSPROMOTED.xhtml`
  puts "Made #{bookChapter}-9HEADERSPROMOTED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{math} -s:../outputs/#{actualBookName}/#{bookChapter}-9HEADERSPROMOTED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-10MATH.xhtml`
  puts "Made #{bookChapter}-10MATH.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{finalrinse} -s:../outputs/#{actualBookName}/#{bookChapter}-10MATH.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-11RINSED.xhtml`
  puts "Made #{bookChapter}-11RINSED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{ucptext} -s:../outputs/#{actualBookName}/#{bookChapter}-11RINSED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-12UCPTEXTED.xhtml`
  puts "Made #{bookChapter}-12UCPTEXTED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{ucpmap} -s:../outputs/#{actualBookName}/#{bookChapter}-12UCPTEXTED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-13UCPMAPPED.xhtml`
  puts "Made #{bookChapter}-13UCPMAPPED.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{splitonbr} -s:../outputs/#{actualBookName}/#{bookChapter}-13UCPMAPPED.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-14SPLITONBR.xhtml`
  puts "Made #{bookChapter}-14SPLITONBR.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{editoriabasic} -s:../outputs/#{actualBookName}/#{bookChapter}-14SPLITONBR.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-15EDITORIABASIC.xhtml`
  puts "Made #{bookChapter}-15EDITORIABASIC.xhtml"
  
  %x`#{commandname} #{saxonHE} -xsl:#{editoriareduce} -s:../outputs/#{actualBookName}/#{bookChapter}-15EDITORIABASIC.xhtml -o:../outputs/#{actualBookName}/#{bookChapter}-16EDITORIAREDUCE.html`
  puts "Made #{bookChapter}-16EDITORIAREDUCE.html"
  
  %x`#{commandname} #{saxonHE} -xsl:#{xmltohtml5} -s:../outputs/#{actualBookName}/#{bookChapter}-16EDITORIAREDUCE.html -o:../outputs/#{actualBookName}/#{bookChapter}-17HTML5.html`
  puts "Made #{bookChapter}-17HTML5.html"
  
end


# starting = Time.now
xsweet_script_path = Dir.glob("XSweet-*").pop

rootDir = Dir.getwd
convertDir = rootDir + "/to_convert"

book_list = Dir["#{convertDir}/*"].select {|cand| File.directory? cand}
book_list.delete("#{convertDir}/temp")

puts "BOOK LIST"
number_label = 1
book_list.each do |book|
  puts "#{number_label}: #{book}"
  number_label += 1
end
puts "BOOK LIST END"

book_list.each do |book|
  book = book.split("/").last
  puts "BOOK: #{book}"

  file_path_list = Dir["#{convertDir}/#{book}/*"].select {|cand| File.directory? cand}
  # puts file_path_list

  file_list = []
  file_path_list.each do |file_path|
    file_list << file_path.split("/").last
  end

  newDir = rootDir + "/#{xsweet_script_path}/scripts"

  Dir.chdir newDir

  file_list.each do |chapter|
    puts "converting: #{chapter}"
    execute_chain("#{convertDir}/#{book}", book, chapter)
    #%x`sh execute_chain.sh #{convertDir}/#{book} #{book} #{chapter}`
  end
  puts "done with #{book}"
end
# ending = Time.now
# elapsed = ending - starting
# puts elapsedls

