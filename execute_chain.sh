#!/bin/bash
# For producing HTML5 outputs via XSweet XSLT from sources extracted from .docx (Office Open XML)

# This script is most easily invoked via the `xsweet_runner.rb` script, which will iterate through
# unzipped .docx files in a conversion directory. See https://gitlab.coko.foundation/XSweet/XSweet_runner_scripts
# for README.

# Run bash script by itself as: `sh execute_chain.sh [path_to_conversion_folder] [bookname_dir] [docx_filename]`

# 1st arg: path to a 'to_convert' directory
P=$1
# 2nd arg: name of book directory within 'to_convert' directory, e.g. "great_expectations"
BOOKNAME=$2
# 3rd arg: name of .docx file to convert, e.g. "1_introduction.docx"
DOCNAME=$3

# Note Saxon is included with this distribution, qv for license.
saxonHE="java -jar ../lib/SaxonHE9-9-1-1J/saxon9he.jar"  # SaxonHE (XSLT 3.0 processor)

# INITIAL EXTRACTION
EXTRACT="../applications/docx-extract/docx-html-extract.xsl"
  # RUNS `docx-table-extract.xsl`
NOTES="../applications/docx-extract/handle-notes.xsl"
SCRUB="../applications/docx-extract/scrub.xsl"
JOIN="../applications/docx-extract/join-elements.xsl"
COLLAPSEPARA="../applications/docx-extract/collapse-paragraphs.xsl"

# TAG PLAIN TEXT STRINGS THAT APPEAR TO BE URLS AS LINKS
LINKS="../applications/htmlevator/applications/hyperlink-inferencer/hyperlink-inferencer.xsl"

# REBUILDS LISTS FROM WORD AS HTML
PROMOTELISTS="../applications/list-promote/PROMOTE-lists.xsl"
  # RUNS `mark-lists.xsl`, THEN `itemize-lists.xsl`

# DETECTS PLAIN-TEXT NUMBERED LISTS
DETECTLISTS="../applications/htmlevator/applications/list-detect/DETECT-ITEMIZE-LISTS.xsl"
  # RUNS `detect-numbered-lists.xsl`, THEN `itemize-detected-lists.xsl`, THEN `scrub-literal-numbering-lists.xsl`

# HEADER PROMOTION BY OUTLINE LEVEL OR DISPLAY ATTRIBUTES
HEADERCHOOSEANDPROMOTE="../applications/htmlevator/applications/header-promote/header-promotion-CHOOSE.xsl"

# MATH (OMML TO MML)
MATH="../applications/math/xsweet_tei_omml2mml.xsl"

# FINAL HTML CLEANUPS
FINALRINSE="../applications/html-polish/final-rinse.xsl"

# UNIVERSITY OF CALIFORNIA PRESS COPYEDITING CLEANUPS
UCPTEXT="../applications/htmlevator/applications/ucp-cleanup/ucp-text-macros.xsl"

# UCP-SPECIFIC SPECIFIC ELEMENT MAPPING
UCPMAP="../applications/htmlevator/applications/ucp-cleanup/ucp-mappings.xsl"

# EDITORIA TYPESCRIPT
SPLITONBR="../applications/typescript/p-split-around-br.xsl"
EDITORIABASIC="../applications/typescript/editoria-basic.xsl"
EDITORIAREDUCE="../applications/typescript/editoria-reduce.xsl"

# SERIALIZE TO HTML5
XMLTOHTML5="../applications/html-polish/html5-serialize.xsl"


# Intermediate and final outputs (serializations) are all left on the file system.

$saxonHE -xsl:$EXTRACT -s:$P/$DOCNAME/word/document.xml -o:../outputs/$BOOKNAME/$DOCNAME-1EXTRACTED.xhtml
echo Made $DOCNAME-1EXTRACTED.xhtml

$saxonHE -xsl:$NOTES -s:../outputs/$BOOKNAME/$DOCNAME-1EXTRACTED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-2NOTES.xhtml
echo Made $DOCNAME-2NOTES.xhtml

$saxonHE -xsl:$SCRUB -s:../outputs/$BOOKNAME/$DOCNAME-2NOTES.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-3SCRUBBED.xhtml
echo Made $DOCNAME-3SCRUBBED.xhtml

$saxonHE -xsl:$JOIN -s:../outputs/$BOOKNAME/$DOCNAME-3SCRUBBED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-4JOINED.xhtml
echo Made $DOCNAME-4JOINED.xhtml

$saxonHE -xsl:$COLLAPSEPARA -s:../outputs/$BOOKNAME/$DOCNAME-4JOINED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-5COLLAPSED.xhtml
echo Made $DOCNAME-5COLLAPSED.xhtml

$saxonHE -xsl:$LINKS -s:../outputs/$BOOKNAME/$DOCNAME-5COLLAPSED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-6LINKS.xhtml
echo Made $DOCNAME-6LINKS.xhtml

$saxonHE -xsl:$PROMOTELISTS -s:../outputs/$BOOKNAME/$DOCNAME-6LINKS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-7PROMOTELISTS.xhtml
echo Made $DOCNAME-7PROMOTELISTS.xhtml

$saxonHE -xsl:$DETECTLISTS -s:../outputs/$BOOKNAME/$DOCNAME-7PROMOTELISTS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-8DETECTLISTS.xhtml
echo Made $DOCNAME-8DETECTLISTS.xhtml

$saxonHE -xsl:$HEADERCHOOSEANDPROMOTE -s:../outputs/$BOOKNAME/$DOCNAME-8DETECTLISTS.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-9HEADERSPROMOTED.xhtml
echo Made $DOCNAME-9HEADERSPROMOTED.xhtml

$saxonHE -xsl:$MATH -s:../outputs/$BOOKNAME/$DOCNAME-9HEADERSPROMOTED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-10MATH.xhtml
echo Made $DOCNAME-10MATH.xhtml

$saxonHE -xsl:$FINALRINSE -s:../outputs/$BOOKNAME/$DOCNAME-10MATH.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-11RINSED.xhtml
echo Made $DOCNAME-11RINSED.xhtml

$saxonHE -xsl:$UCPTEXT -s:../outputs/$BOOKNAME/$DOCNAME-11RINSED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-12UCPTEXTED.xhtml
echo Made $DOCNAME-12UCPTEXTED.xhtml

$saxonHE -xsl:$UCPMAP -s:../outputs/$BOOKNAME/$DOCNAME-12UCPTEXTED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-13UCPMAPPED.xhtml
echo Made $DOCNAME-13UCPMAPPED.xhtml

$saxonHE -xsl:$SPLITONBR -s:../outputs/$BOOKNAME/$DOCNAME-13UCPMAPPED.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-14SPLITONBR.xhtml
echo Made $DOCNAME-14SPLITONBR.xhtml

$saxonHE -xsl:$EDITORIABASIC -s:../outputs/$BOOKNAME/$DOCNAME-14SPLITONBR.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-15EDITORIABASIC.xhtml
echo Made $DOCNAME-15EDITORIABASIC.xhtml

$saxonHE -xsl:$EDITORIAREDUCE -s:../outputs/$BOOKNAME/$DOCNAME-15EDITORIABASIC.xhtml -o:../outputs/$BOOKNAME/$DOCNAME-16EDITORIAREDUCE.html
echo Made $DOCNAME-16EDITORIAREDUCE.html

$saxonHE -xsl:$XMLTOHTML5 -s:../outputs/$BOOKNAME/$DOCNAME-16EDITORIAREDUCE.html -o:../outputs/$BOOKNAME/$DOCNAME-17HTML5.html
echo Made $DOCNAME-17HTML5.html
